
locals {
  mandatory_tag = {
    project        = "conilius"
    application    = "ELASTICSEARCH"
    builder        = "CONILIUS"
    Ower           = "CONILIUS"
    Group          = "CFA"
    component_name = "cfa"
  }
  vpc_id          = data.aws_vpc.default_vpc.id
  subnet_ids      = ["subnet-0343af798cd771986", "subnet-061507f98a825b661"]
  security_groups = []
}
data "aws_vpc" "default_vpc" {
  id = var.vpc_id
}

module "elasticsearch" {
  source = "../../"

  #security_groups                = local.security_groups
  vpc_id                   = local.vpc_id
  subnet_ids               = local.subnet_ids
  zone_awareness_enabled   = var.zone_awareness_enabled
  elasticsearch_version    = var.elasticsearch_version
  instance_type            = var.instance_type
  instance_count           = var.instance_count
  encrypt_at_rest_enabled  = var.encrypt_at_rest_enabled
  dedicated_master_enabled = var.dedicated_master_enabled
  kibana_subdomain_name    = var.kibana_subdomain_name
  ebs_volume_size          = var.ebs_volume_size
  kibana_hostname_enabled  = var.kibana_hostname_enabled
  domain_hostname_enabled  = var.domain_hostname_enabled

  domain = "elasticsearch"
  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }
}
